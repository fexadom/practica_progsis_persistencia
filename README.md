# Práctica 5: Uso de persistencia en C con fprintf y fscanf #

Este es un repositorio esqueleto para la práctica 5 de la materia Programación de Sistemas (CCPG1008) P1 de la ESPOL.
Práctica introductoria al uso de Git como herramienta de gestión de proyectos y al uso de persistencia con fprintf y fscanf. 

### ¿Cómo empiezo? ###

* Hacer un fork de este repositorio a su cuenta personal de Bitbucket (una cuenta por grupo)
* Clonar el repositorio en su cuenta (no este) en su computadora del laboratorio
* Completar la práctica en grupo
* Haga commit y push a su trabajo
* El entregable es un enlace al repositorio

### Integrantes ###

Edite esta lista y añada los nombres de los integrantes, luego borre esta línea.

* Integrante 1
* Integrante 2
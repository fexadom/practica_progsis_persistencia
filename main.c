#include <stdio.h>
#include "medicion.h"

int main()
{
	FILE *fp;
	char filename[MAXLINE];
	Medicion_t m;

	printf("Ingrese nommbre archivo: ");
	scanf("%s",filename);

	fp = fopen(filename,"r");

	//Salta la primera linea (cabecera del archivo)
	fscanf(fp,"%*[^\n]\n");

	//Lee la segunda linea
	int n = fscanf(fp, "%s\t%s\t%f\t%f\t%d\t%f\t%f", m.date, m.time, &m.temperature, &m.humidity, &m.pressure, &m.altitude, &m.battery);

	//Si retorna 7 o mas variables entonces se ha leido correctamente la linea
	if(n >= 7)
		printf("%s, %s, %f, %f, %d, %f, %f\n", m.date, m.time, m.temperature, m.humidity, m.pressure, m.altitude, m.battery);
	else
		fprintf(stderr,"Error al leer el archivo\n");

	fclose(fp);
}

